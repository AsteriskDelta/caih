#ifndef CAIH_INCLUDE
#define CAIH_INCLUDE

#include <Spinster/Spinster.h>

#include <ARKE/ARKE.h>
#include <ARKE/Application.h>
#include <ARKE/Console.h>
namespace Application = arke::Application;
using arke::Console;


#include <NVX/NVX.h>
//#include <ARKE/EngineClient.h>
//#include <ARKE/UI.h>

#include <mutex>

//#include <TMSC/TMSCInclude.h>
//#include <TMSC/TMSCFundamental.h>
//#include <TMSC/render/DebugGroup.h>


#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <cstdint>

using byte=std::byte;
using ptr_t=std::ptrdiff_t;

namespace CAIH {
    using namespace arke;
};

#ifndef _instantiator
#define _instantiator weak
#endif 

#ifndef _mutate
#define _mutate(FN, ARGS, VARS) \
inline auto FN ARGS {\
    const auto *const cthis = &const_cast<const decltype(*this)&>(*this);\
    using RetType = typename std::remove_const<typename std::remove_reference<decltype(cthis->FN VARS)>::type>::type;\
    return const_cast<RetType>(cthis->FN VARS);\
}
#define _rmutate(FN, ARGS, VARS) \
inline auto FN ARGS {\
    const auto *const cthis = &const_cast<const decltype(*this)&>(*this);\
    using RetType = typename std::remove_const<typename std::remove_reference<decltype(cthis->FN VARS)>::type>::type;\
    /*std::cout << decltype(const_cast<RetType&>(cthis->FN VARS))::blah << "\n";*/\
    /*std::cout << "RMT " << __PRETTY_FUNCTION__ << "\n";*/\
    return const_cast<RetType&>(cthis->FN VARS);\
}
#endif

#ifndef _getter
#define _getter(VAR) \
inline const auto& VAR() const {\
    return VAR##_;\
}
#define _setter(VAR) \
inline const auto& VAR(const decltype(VAR##_)& nVar) {\
    VAR##_ = nVar;\
    return VAR##_;\
}
#define _setter_cb(VAR, CB) \
inline const auto& VAR(const decltype(VAR##_)& nVar) {\
    VAR##_ = nVar;\
    CB();\
    return VAR##_;\
}
#define _getset(VAR) \
_getter(VAR);\
_setter(VAR);

#define _getters(...) APPLYXn(_getter, __VA_ARGS__)
#define _setters(...) APPLYXn(_setter, __VA_ARGS__)
#define _getseters(...) APPLYXn(_getset, __VA_ARGS__)
#endif


#include <NVX/NVX.h>
#include <ARKE/Time.h>
namespace C3AI {
    using namespace nvx;
    typedef double Num;
    //typedef Num Time;
    using arke::Time;
    
    class Core;
    class AI;
    class Muse;
    class Stream;
    class SymbolSet;
    class CriticSet;
    
    class Symbol;
    class Region;
    class Symlink;
    class Intrinsic;
    class Interface;
    class Critic;
    class ExtLibrary;
    
    typedef Symbol* SymbolPtr;
    typedef Region* RegionPtr;
    typedef Critic* CriticPtr;
    
    //extern thread_local ExtLibrary *CurrentExtLibrary;
};

extern Spin::Group WorkGroup;

#ifndef _isnan
#define _isnan(val) (\
    ((val <= 0) == (val > 0))\
    )
#endif

#ifndef STR_MULTIREP
#define STR_MULTIREP
#include <string>
#include <algorithm>
namespace std {
    inline std::string& replace_all(std::string& str, const std::string& from, const std::string& to) {
        std::size_t start_pos = 0;
        while((start_pos = str.find(from, start_pos)) != std::string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // ...
        }
        return str;
    }
    
    inline std::string get_last_delim(const std::string& s, const std::string& delim) {
        auto index = s.find_last_of(delim);
        return s.substr(++index);
    }
};
#endif

#endif
